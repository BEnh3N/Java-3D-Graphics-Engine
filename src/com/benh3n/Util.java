package com.benh3n;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import com.benh3n.structs.Triangle;
import com.benh3n.structs.Vec2D;
import com.benh3n.structs.Vec3D;

public final class Util {
    private Util() {
    }

    public static class mat4x4 {
        float[][] m = new float[4][4];
    }
    public static class returnClip {
        int numTris;
        Triangle[] tris;
        public returnClip(int numTris, Triangle[] tris) {
            this.numTris = numTris;
            this.tris = tris;
        }
    }

    public static Vec3D MatrixMultiplyVector(mat4x4 m, Vec3D i) {
        Vec3D v = new Vec3D();
        v.setX(i.getX() * m.m[0][0] + i.getY() * m.m[1][0] + i.getZ() * m.m[2][0] + i.getW() * m.m[3][0]);
        v.setY(i.getX() * m.m[0][1] + i.getY() * m.m[1][1] + i.getZ() * m.m[2][1] + i.getW() * m.m[3][1]);
        v.setZ(i.getX() * m.m[0][2] + i.getY() * m.m[1][2] + i.getZ() * m.m[2][2] + i.getW() * m.m[3][2]);
        v.setW(i.getX() * m.m[0][3] + i.getY() * m.m[1][3] + i.getZ() * m.m[2][3] + i.getW() * m.m[3][3]);
        return v;
    }
    public static mat4x4 MatrixMakeIdentity() {
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = 1.0f;
        matrix.m[1][1] = 1.0f;
        matrix.m[2][2] = 1.0f;
        matrix.m[3][3] = 1.0f;
        return matrix;
    }
    public static mat4x4 MatrixMakeRotationX(float fAngleRad) {
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = 1.0f;
        matrix.m[1][1] = (float) Math.cos(fAngleRad);
        matrix.m[1][2] = (float) Math.sin(fAngleRad);
        matrix.m[2][1] = (float) -Math.sin(fAngleRad);
        matrix.m[2][2] = (float) Math.cos(fAngleRad);
        matrix.m[3][3] = 1.0f;
        return matrix;
    }
    public static mat4x4 MatrixMakeRotationY(float fAngleRad) {
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = (float) Math.cos(fAngleRad);
        matrix.m[0][2] = (float) Math.sin(fAngleRad);
        matrix.m[2][0] = (float) -Math.sin(fAngleRad);
        matrix.m[1][1] = 1.0f;
        matrix.m[2][2] = (float) Math.cos(fAngleRad);
        matrix.m[3][3] = 1.0f;
        return matrix;
    }
    public static mat4x4 MatrixMakeRotationZ(float fAngleRad) {
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = (float) Math.cos(fAngleRad);
        matrix.m[0][1] = (float) Math.sin(fAngleRad);
        matrix.m[1][0] = (float) -Math.sin(fAngleRad);
        matrix.m[1][1] = (float) Math.cos(fAngleRad);
        matrix.m[2][2] = 1.0f;
        matrix.m[3][3] = 1.0f;
        return matrix;
    }
    public static mat4x4 MatrixMakeTranslation(float x, float y, float z) {
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = 1.0f;
        matrix.m[1][1] = 1.0f;
        matrix.m[2][2] = 1.0f;
        matrix.m[3][3] = 1.0f;
        matrix.m[3][0] = x;
        matrix.m[3][1] = y;
        matrix.m[3][2] = z;
        return matrix;
    }
    public static mat4x4 MatrixMakeProjection(float fFovDegrees, float fAspectRatio, float fNear, float fFar) {
        float fFovRad = 1.0f / (float) Math.tan(fFovDegrees * 0.5f / 180.0f * 3.14159f);
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = fAspectRatio * fFovRad;
        matrix.m[1][1] = fFovRad;
        matrix.m[2][2] = fFar / (fFar - fNear);
        matrix.m[3][2] = (-fFar * fNear) / (fFar - fNear);
        matrix.m[2][3] = 1.0f;
        matrix.m[3][3] = 0.0f;
        return matrix;
    }
    public static mat4x4 MatrixMultiplyMatrix(mat4x4 m1, mat4x4 m2) {
        mat4x4 matrix = new mat4x4();
        for (int c = 0; c < 4; c++) {
            for (int r = 0; r < 4; r++) {
                matrix.m[r][c] = m1.m[r][0] * m2.m[0][c] + m1.m[r][1] * m2.m[1][c] + m1.m[r][2] * m2.m[2][c] + m1.m[r][3] * m2.m[3][c];
            }
        }
        return matrix;
    }
    public static mat4x4 MatrixPointAt(Vec3D pos, Vec3D target, Vec3D up) {
        // Calculate new forward direction
        Vec3D newForward = VectorSub(target, pos);
        newForward = VectorNormalise(newForward);

        // Calculate new up direction
        Vec3D a = VectorMul(newForward, VectorDotProduct(up, newForward));
        Vec3D newUp = VectorSub(up, a);
        newUp = VectorNormalise(newUp);

        // New Right direction is easy, its just cross product
        Vec3D newRight = VectorCrossProduct(newUp, newForward);

        // Construct Dimensioning and Translation Matrix
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = newRight.getX();	matrix.m[0][1] = newRight.getY();	matrix.m[0][2] = newRight.getZ();	matrix.m[0][3] = 0.0f;
        matrix.m[1][0] = newUp.getX();		matrix.m[1][1] = newUp.getY();		matrix.m[1][2] = newUp.getZ();		matrix.m[1][3] = 0.0f;
        matrix.m[2][0] = newForward.getX();	matrix.m[2][1] = newForward.getY();	matrix.m[2][2] = newForward.getZ();	matrix.m[2][3] = 0.0f;
        matrix.m[3][0] = pos.getX();		matrix.m[3][1] = pos.getY();		matrix.m[3][2] = pos.getZ();		matrix.m[3][3] = 1.0f;
        return matrix;
    }
    public static mat4x4 MatrixQuickInverse(mat4x4 m) { // Only for Rotation/Translation Matrices
        mat4x4 matrix = new mat4x4();
        matrix.m[0][0] = m.m[0][0]; matrix.m[0][1] = m.m[1][0]; matrix.m[0][2] = m.m[2][0]; matrix.m[0][3] = 0.0f;
        matrix.m[1][0] = m.m[0][1]; matrix.m[1][1] = m.m[1][1]; matrix.m[1][2] = m.m[2][1]; matrix.m[1][3] = 0.0f;
        matrix.m[2][0] = m.m[0][2]; matrix.m[2][1] = m.m[1][2]; matrix.m[2][2] = m.m[2][2]; matrix.m[2][3] = 0.0f;
        matrix.m[3][0] = -(m.m[3][0] * matrix.m[0][0] + m.m[3][1] * matrix.m[1][0] + m.m[3][2] * matrix.m[2][0]);
        matrix.m[3][1] = -(m.m[3][0] * matrix.m[0][1] + m.m[3][1] * matrix.m[1][1] + m.m[3][2] * matrix.m[2][1]);
        matrix.m[3][2] = -(m.m[3][0] * matrix.m[0][2] + m.m[3][1] * matrix.m[1][2] + m.m[3][2] * matrix.m[2][2]);
        matrix.m[3][3] = 1.0f;
        return matrix;
    }

    public static Vec3D VectorAdd(Vec3D v1, Vec3D v2) {
        return new Vec3D(v1.getX() + v2.getX(), v1.getY() + v2.getY(), v1.getZ() + v2.getZ());
    }
    public static Vec3D VectorSub(Vec3D v1, Vec3D v2) {
        return new Vec3D(v1.getX() - v2.getX(), v1.getY() - v2.getY(), v1.getZ() - v2.getZ());
    }
    public static Vec3D VectorMul(Vec3D v1, float k) {
        return new Vec3D(v1.getX() * k, v1.getY() * k, v1.getZ() * k);
    }
    public static Vec3D VectorDiv(Vec3D v1, float k) {
        return new Vec3D(v1.getX() / k, v1.getY() / k, v1.getZ() / k );
    }
    public static float VectorDotProduct(Vec3D v1, Vec3D v2) {
        return v1.getX() * v2.getX() + v1.getY() * v2.getY() + v1.getZ() * v2.getZ();
    }
    public static float VectorLength(Vec3D v) {
        return (float) Math.sqrt(VectorDotProduct(v, v));
    }
    public static Vec3D VectorNormalise(Vec3D v) {
        float l = VectorLength(v);
        return new Vec3D(v.getX() / l, v.getY() / l, v.getZ() / l);
    }
    public static Vec3D VectorCrossProduct(Vec3D v1, Vec3D v2) {
        Vec3D v = new Vec3D();
        v.setX(v1.getY() * v2.getZ() - v1.getZ() * v2.getY());
        v.setY(v1.getZ() * v2.getX() - v1.getX() * v2.getZ());
        v.setZ(v1.getX() * v2.getY() - v1.getY() * v2.getX());
        return v;
    }
    public static Vec3D VectorIntersectPlane(Vec3D planeP, Vec3D planeN, Vec3D lineStart, Vec3D lineEnd, float[] t) {
        planeN = VectorNormalise(planeN);
        float planeD = -VectorDotProduct(planeN, planeP);
        float ad = VectorDotProduct(lineStart, planeN);
        float bd = VectorDotProduct(lineEnd, planeN);
        t[0] = (-planeD - ad) / (bd - ad);
        Vec3D lineStartToEnd = VectorSub(lineEnd, lineStart);
        Vec3D lineToIntersect = VectorMul(lineStartToEnd, t[0]);
        return VectorAdd(lineStart, lineToIntersect);
    }

    interface Dist {
        float dist(Vec3D p);
    }
    public static returnClip TriangleClipAgainstPlane(Vec3D planeP, Vec3D planeN, Triangle inTri) {
        Triangle outTri1 = new Triangle();
        Triangle outTri2 = new Triangle();

        // Make sure plane normal is indeed normal
        planeN = VectorNormalise(planeN);

        // Return signed shortest distance from point to plane, place normal must be normalised
        Vec3D finalPlaneN = planeN;
        Dist d = (Vec3D p) -> {
            // Vec3D n = VectorNormalise(p);
            return finalPlaneN.getX() * p.getX() + finalPlaneN.getY() * p.getY() + finalPlaneN.getZ() * p.getZ() - VectorDotProduct(finalPlaneN, planeP);
        };

        // Create two temporary storage arrays to classify points either side of plane
        // If distance sign is positive, point lies on "inside" of plane
        Vec3D[] insidePoints  = new Vec3D[3]; int nInsidePointCount  = 0;
        Vec3D[] outsidePoints = new Vec3D[3]; int nOutsidePointCount = 0;
        Vec2D[] insideTex  = new Vec2D[3];
        Vec2D[] outsideTex = new Vec2D[3];

        // Get signed distance of each point in Triangle to plane
        float d0 = d.dist(inTri.p[0]);
        float d1 = d.dist(inTri.p[1]);
        float d2 = d.dist(inTri.p[2]);

        if (d0 >= 0) {
            insidePoints[nInsidePointCount] = inTri.p[0]; insideTex[nInsidePointCount++] = inTri.t[0];
        }
        else {
            outsidePoints[nOutsidePointCount] = inTri.p[0]; outsideTex[nOutsidePointCount++] = inTri.t[0];
        }
        if (d1 >= 0) {
            insidePoints[nInsidePointCount] = inTri.p[1]; insideTex[nInsidePointCount++] = inTri.t[1];
        }
        else {
            outsidePoints[nOutsidePointCount] = inTri.p[1]; outsideTex[nOutsidePointCount++] = inTri.t[1];
        }
        if (d2 >= 0) {
            insidePoints[nInsidePointCount] = inTri.p[2]; insideTex[nInsidePointCount++] = inTri.t[2];
        }
        else {
            outsidePoints[nOutsidePointCount] = inTri.p[2]; outsideTex[nOutsidePointCount++] = inTri.t[2];
        }

        // Now classify Triangle points, and break the input Triangle into
        // smaller output Triangles if required. There are four possible
        // outcomes...

        if (nInsidePointCount == 0) {
            // All points lie on the outside of plane, so clip whole Triangle
            // It ceases to exist

            return new returnClip(0, new Triangle[]{null, null}); // No returned Triangles are valid

        } else if (nInsidePointCount == 3) {
            // All points lie on the inside of plane, so do nothing
            // and allow the Triangle to simply pass through
            outTri1 = inTri;

            return new returnClip(1, new Triangle[]{outTri1.clone(), null}); // Just the one returned original Triangle is valid

        } else if (nOutsidePointCount == 2) {
            // Triangle should be clipped. As two points lie outside
            // the plane, the Triangle simply becomes a smaller Triangle

            // Copy appearance info to new Triangle
            outTri1.col = inTri.col;
//            outTri1.col = Color.BLUE;

            // The inside point is valid, so keep that...
            outTri1.p[0] = insidePoints[0];
            outTri1.t[0] = insideTex[0];

            // but the two new points are at the locations where the
            // original sides of the Triangle (lines) intersect with the plane
            float[] t = {0};
            outTri1.p[1] = VectorIntersectPlane(planeP, planeN, insidePoints[0], outsidePoints[0], t);
            outTri1.t[1].setU(t[0] * (outsideTex[0].getU() - insideTex[0].getU()) + insideTex[0].getU());
            outTri1.t[1].setV(t[0] * (outsideTex[0].getV() - insideTex[0].getV()) + insideTex[0].getV());

            outTri1.p[2] = VectorIntersectPlane(planeP, planeN, insidePoints[0], outsidePoints[1], t);
            outTri1.t[2].setU(t[0] * (outsideTex[1].getU() - insideTex[0].getU()) + insideTex[0].getU());
            outTri1.t[2].setV(t[0] * (outsideTex[1].getV() - insideTex[0].getV()) + insideTex[0].getV());

            return new returnClip(1, new Triangle[]{outTri1, null}); // Return the newly formed single Triangle

        } else {
            // Triangle should be clipped. As two points lie inside the plane,
            // the clipped Triangle becomes a "quad". Fortunately, we can
            // represent a quad with two new Triangles

            // Copy appearance info to new Triangles
            outTri1.col =  inTri.col;
            outTri2.col =  inTri.col;
//            outTri1.col = Color.GREEN;
//            outTri2.col = Color.RED;

            // The first Triangle consists of the two inside points and a new
            // point determined by the location where one side of the Triangle
            // intersects with the plane
            outTri1.p[0] = insidePoints[0];
            outTri1.p[1] = insidePoints[1];
            outTri1.t[0] = insideTex[0];
            outTri1.t[1] = insideTex[1];

            float[] t = {0};
            outTri1.p[2] = VectorIntersectPlane(planeP, planeN, insidePoints[0], outsidePoints[0], t);
            outTri1.t[2].setU(t[0] * (outsideTex[0].getU() - insideTex[0].getU()) + insideTex[0].getU());
            outTri1.t[2].setV(t[0] * (outsideTex[0].getV() - insideTex[0].getV()) + insideTex[0].getV());

            // The second Triangle is composed of one of the inside points, a
            // new point determined by the intersection of the other side of the
            // Triangle and the plane, and the newly created point above
            outTri2.p[0] = insidePoints[1];
            outTri2.t[0] = insideTex[1];
            outTri2.p[1] = outTri1.p[2];
            outTri2.t[1] = outTri1.t[2];
            outTri2.p[2] = VectorIntersectPlane(planeP, planeN, insidePoints[1], outsidePoints[0], t);
            outTri2.t[2].setU(t[0] * (outsideTex[0].getU() - insideTex[1].getU()) + insideTex[1].getU());
            outTri2.t[2].setV(t[0] * (outsideTex[0].getV() - insideTex[1].getV()) + insideTex[1].getV());

            return new returnClip(2, new Triangle[]{outTri1, outTri2}); // Return two newly formed triangles which form a quad
        }
    }

    public static Color getColor(float lum){
        int col = (int)Math.abs(lum * 255);
        return new Color(col, col, col);
    }

    public static void TexturedTriangle(ArrayList<Integer> x, ArrayList<Integer> y, ArrayList<Float> u, ArrayList<Float> v, ArrayList<Float> w,
                                        Graphics2D g2d, BufferedImage tex) {

        if (y.get(1) < y.get(0)) {
            Collections.swap(y, 0, 1);
            Collections.swap(x, 0, 1);
            Collections.swap(u, 0, 1);
            Collections.swap(v, 0, 1);
            Collections.swap(w, 0, 1);
        }
        if (y.get(2) < y.get(0)) {
            Collections.swap(y, 0, 2);
            Collections.swap(x, 0, 2);
            Collections.swap(u, 0, 2);
            Collections.swap(v, 0, 2);
            Collections.swap(w, 0, 2);
        }
        if (y.get(2) < y.get(1)) {
            Collections.swap(y, 1, 2);
            Collections.swap(x, 1, 2);
            Collections.swap(u, 1, 2);
            Collections.swap(v, 1, 2);
            Collections.swap(w, 1, 2);
        }


        int dy1 = y.get(1) - y.get(0);
        int dx1 = x.get(1) - x.get(0);
        float dv1 = v.get(1) - v.get(0);
        float du1 = u.get(1) - u.get(0);
//        float dw1 = w.get(1) - w.get(0);

        int dy2 = y.get(2) - y.get(0);
        int dx2 = x.get(2) - x.get(0);
        float dv2 = v.get(2) - v.get(0);
        float du2 = u.get(2) - u.get(0);
//        float dw2 = w.get(2) - w.get(0);

        float texU, texV;

        float daxStep = 0, dbxStep = 0,
                du1Step = 0, dv1Step = 0,
                du2Step = 0, dv2Step = 0;

        if (dy1 != 0) daxStep = dx1 / (float)Math.abs(dy1);
        if (dy2 != 0) dbxStep = dx2 / (float)Math.abs(dy2);

        if (dy1 != 0) du1Step = du1 / (float)Math.abs(dy1);
        if (dy1 != 0) dv1Step = dv1 / (float)Math.abs(dy1);

        if (dy2 != 0) du2Step = du2 / (float)Math.abs(dy2);
        if (dy2 != 0) dv2Step = dv2 / (float)Math.abs(dy2);

        if (dy1 != 0) {
            for (int i = y.get(0); i <= y.get(1); i++) {

                int ax = (int) (x.get(0) + (float)(i - y.get(0)) * daxStep);
                int bx = (int) (x.get(0) + (float)(i - y.get(0)) * dbxStep);

                float texSu = u.get(0) + (float)(i - y.get(0)) * du1Step;
                float texSv = v.get(0) + (float)(i - y.get(0)) * dv1Step;

                float texEu = u.get(0) + (float)(i - y.get(0)) * du2Step;
                float texEv = v.get(0) + (float)(i - y.get(0)) * dv2Step;

                if (ax > bx) {
                    int swap1 = ax; ax = bx; bx = swap1;
                    float swap2 = texSu; texSu = texEu; texEu = swap2;
                    float swap3 = texSv; texSv = texEv; texEv = swap3;
                }

                texU = texSu;
                texV = texSv;

                float tStep = 1.0f / (float)(bx - ax);
                float t = 0.0f;

                for (int j = ax; j < bx; j++) {
                    texU = (1.0f - t) * texSu + t * texEu;
                    texV = (1.0f - t) * texSv + t * texEv;

                    int color = tex.getRGB((int)texU, (int)texV);
                    g2d.setColor(new Color((color & 0xff0000) >> 16, (color & 0xff00) >> 8, color & 0xff));
                    g2d.drawLine(j, i, j, i);

                    t += tStep;
                }
            }

            dy1 = y.get(2) - y.get(1);
            dx1 = x.get(2) - x.get(1);
            dv1 = v.get(2) - v.get(1);
            du1 = u.get(2) - u.get(1);

            if (dy1 != 0) daxStep = dx1 / (float)Math.abs(dy1);
            if (dy2 != 0) dbxStep = dx2 / (float)Math.abs(dy2);

            du1Step = 0; dv1Step = 0;
            if (dy1 != 0) du1Step = du1 / (float)Math.abs(dy1);
            if (dy1 != 0) dv1Step = dv1 / (float)Math.abs(dy1);

            for (int i = y.get(1); i <= y.get(2); i++) {
                int ax = (int) (x.get(1) + (i - y.get(1)) * daxStep);
                int bx = (int) (x.get(0) + (i - y.get(0)) * dbxStep);

                float texSu = u.get(1) + (i - y.get(1)) * du1Step;
                float texSv = v.get(1) + (i - y.get(1)) * dv1Step;

                float texEu = u.get(0) + (i - y.get(0)) * du2Step;
                float texEv = v.get(0) + (i - y.get(0)) * dv2Step;

                if (ax > bx) {
                    int swap1 = ax; ax = bx; bx = swap1;
                    float swap2 = texSu; texSu = texEu; texEu = swap2;
                    float swap3 = texSv; texSv = texEv; texEv = swap3;
                }

                float tStep = 1.0f / (float)(bx - ax);
                float t = 0.0f;

                for (int j = ax; j < bx; j++) {
                    texU = (1.0f - t) * texSu + t * texEu;
                    texV = (1.0f - t) * texSv + t * texEv;

                    int color = tex.getRGB((int)texU, (int)texV);
                    g2d.setColor(new Color((color & 0xff0000) >> 16, (color & 0xff00) >> 8, color & 0xff));
                    g2d.drawLine(j, i, j, i);

                    t += tStep;
                }
            }
        }
    }
}
