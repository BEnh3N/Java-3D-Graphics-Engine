package com.benh3n.structs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Mesh {
    private Triangle[] tris;

    public Mesh() {

    }

    public static Mesh loadObjectFromFile(String path){
        Mesh tempMesh = new Mesh();
        try {
            Scanner in = new Scanner(new File(path));
            ArrayList<Vec3D> vecList = new ArrayList<>();
            ArrayList<Triangle> triList = new ArrayList<>();
            while (in.hasNextLine()) {
                String[] line = in.nextLine().split(" ");
                switch (line[0]) {
                    case "v":
                        float x = Float.parseFloat(line[1]);
                        float y = Float.parseFloat(line[2]);
                        float z = Float.parseFloat(line[3]);
                        vecList.add(new Vec3D(x, y, z));
                        break;

                    case "f":
                        int p0 = Integer.parseInt(line[1]) - 1;
                        int p1 = Integer.parseInt(line[2]) - 1;
                        int p2 = Integer.parseInt(line[3]) - 1;
                        Triangle tri = new Triangle(vecList.get(p0), vecList.get(p1), vecList.get(p2));
                        triList.add(tri);
                        break;
                }
            }
            in.close();
            Triangle[] tempList = new Triangle[triList.size()];
            tempList = triList.toArray(tempList);
            tempMesh.tris = tempList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return tempMesh;
    }

    public Triangle[] getTris() {
        return tris;
    }

    public void setTris(Triangle[] tris) {
        this.tris = tris;
    }
}
