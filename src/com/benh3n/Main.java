package com.benh3n;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import com.benh3n.Util.*;
import com.benh3n.structs.Mesh;
import com.benh3n.structs.Triangle;
import com.benh3n.structs.Vec3D;

public class Main {

    static Mesh meshCube = new Mesh();
    static mat4x4 matProj = new mat4x4();
    static Vec3D vCamera = new Vec3D();
    static Vec3D vLookDir;
    static float fYaw;
    static float fTheta;

    static BufferedImage sprTex1;

    static float elapsedTime;

    static boolean running;

    public static void main(String[] args) {

        BufferedImage img = null;
        try { img = ImageIO.read(new File("icon.png")); } catch (IOException e) { System.out.println("you shouldn't be seeing this... (image load failed)"); }

        JFrame frame = new JFrame("3D Graphics Engine");
        frame.setIconImage(img);
        frame.setIgnoreRepaint(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Input input = new Input(frame);

        Canvas canvas = new Canvas();
        canvas.setIgnoreRepaint(true);
        float scale = 2.5f;
        canvas.setSize((int)(256 * scale), (int)(240 * scale));
        canvas.setBackground(Color.BLACK);

        frame.add(canvas);
        frame.pack();
        frame.setVisible(true);

        canvas.createBufferStrategy(2);
        BufferStrategy buffer = canvas.getBufferStrategy();

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        GraphicsConfiguration gc = gd.getDefaultConfiguration();

        BufferedImage bi = gc.createCompatibleImage(canvas.getWidth(), canvas.getHeight());

        Graphics graphics = null;
        Graphics2D g2d = null;
        Color background = Color.BLACK;

        // Load Object From File
        meshCube.setTris(new Triangle[]{
            // SOUTH
            new Triangle(new float[]{0, 0, 0, 0, 1, 0, 1, 1, 0}, new float[]{0, 1, 0, 0, 1, 0}),
            new Triangle(new float[]{0, 0, 0, 1, 1, 0, 1, 0, 0}, new float[]{0, 1, 1, 0, 1, 1}),
            // EAST
            new Triangle(new float[]{1, 0, 0, 1, 1, 0, 1, 1, 1}, new float[]{0, 1, 0, 0, 1, 0}),
            new Triangle(new float[]{1, 0, 0, 1, 1, 1, 1, 0, 1}, new float[]{0, 1, 1, 0, 1, 1}),
            // NORTH
            new Triangle(new float[]{1, 0, 1, 1, 1, 1, 0, 1, 1}, new float[]{0, 1, 0, 0, 1, 0}),
            new Triangle(new float[]{1, 0, 1, 0, 1, 1, 0, 0, 1}, new float[]{0, 1, 1, 0, 1, 1}),
            // WEST
            new Triangle(new float[]{0, 0, 1, 0, 1, 1, 0, 1, 0}, new float[]{0, 1, 0, 0, 1, 0}),
            new Triangle(new float[]{0, 0, 1, 0, 1, 0, 0, 0, 0}, new float[]{0, 1, 1, 0, 1, 1}),
            // TOP
            new Triangle(new float[]{0, 1, 0, 0, 1, 1, 1, 1, 1}, new float[]{0, 1, 0, 0, 1, 0}),
            new Triangle(new float[]{0, 1, 0, 1, 1, 1, 1, 1, 0}, new float[]{0, 1, 1, 0, 1, 1}),
            // BOTTOM
            new Triangle(new float[]{1, 0, 1, 0, 0, 1, 0, 0, 0}, new float[]{0, 1, 0, 0, 1, 0}),
            new Triangle(new float[]{1, 0, 1, 0, 0, 0, 1, 0, 0}, new float[]{0, 1, 1, 0, 1, 1})
        });

//        meshCube = Mesh.loadObjectFromFile("mountains.obj");

        try {
            sprTex1 = ImageIO.read(new File("rainbow.png"));
        } catch (IOException e) { System.out.println("brush"); }

        running = true;
        while (running) {
            try {
                long now = System.nanoTime();

                // Recalculate Projection Matrix
                matProj = Util.MatrixMakeProjection(90.0f, (float)canvas.getHeight() / (float)canvas.getWidth(), 0.1f, 1000.0f);

                // Clear Screen
                g2d = bi.createGraphics();
                g2d.setBackground(background);
                g2d.setColor(background);
                g2d.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

                // Set up Rotation Matrices
                mat4x4 matRotZ, matRotX;
//                fTheta += 0.15 * elapsedTime; // Uncomment to spin me right round baby
                matRotZ = Util.MatrixMakeRotationZ(fTheta * 0.5f);
                matRotX = Util.MatrixMakeRotationX(fTheta);

                mat4x4 matTrans;
                matTrans = Util.MatrixMakeTranslation(0.0f, 0.0f, 5.0f);

                mat4x4 matWorld;
                // matWorld = MatrixMakeIdentity();
                matWorld = Util.MatrixMultiplyMatrix(matRotZ, matRotX);
                matWorld = Util.MatrixMultiplyMatrix(matWorld, matTrans);

                Vec3D vUp = new Vec3D(0, 1, 0);
                Vec3D vTarget = new Vec3D(0, 0, 1);
                mat4x4 matCameraRot = Util.MatrixMakeRotationY(fYaw);
                vLookDir = Util.MatrixMultiplyVector(matCameraRot, vTarget);
                vTarget = Util.VectorAdd(vCamera, vLookDir);

                mat4x4 matCamera = Util.MatrixPointAt(vCamera, vTarget, vUp);

                // Make matrix view from camera
                mat4x4 matView = Util.MatrixQuickInverse(matCamera);

                // Store Triangles for Rastering Later
                ArrayList<Triangle> trianglesToRaster = new ArrayList<>();

                // Draw Triangles
                for (Triangle tri : meshCube.getTris()) {

//                    Triangle triProjected = new Triangle(), triTransformed = new Triangle(), triViewed = new Triangle();
                    Triangle triTransformed = new Triangle();
                    triTransformed.p[0] = Util.MatrixMultiplyVector(matWorld, tri.p[0]);
                    triTransformed.p[1] = Util.MatrixMultiplyVector(matWorld, tri.p[1]);
                    triTransformed.p[2] = Util.MatrixMultiplyVector(matWorld, tri.p[2]);
                    triTransformed.t = tri.t;

                    // Calculate Triangle Normal
                    Vec3D normal, line1, line2;

                    // Get lines either side of triangle
                    line1 = Util.VectorSub(triTransformed.p[1], triTransformed.p[0]);
                    line2 = Util.VectorSub(triTransformed.p[2], triTransformed.p[0]);

                    // Take Cross Product of lines to get normal to triangle surface
                    normal = Util.VectorCrossProduct(line1, line2);

                    // You Normally need to Normalise a Normal!
                    normal = Util.VectorNormalise(normal);

                    // Get Ray from Triangle to Camera
                    Vec3D vCameraRay = Util.VectorSub(triTransformed.p[0], vCamera);

                    // If ray is aligned with normal, then triangle is visible
                    if(Util.VectorDotProduct(normal, vCameraRay) < 0.0f) {

                        // Illumination
                        Vec3D lightDirection = new Vec3D(0, 1, -1);
                        lightDirection = Util.VectorNormalise(lightDirection);

                        // How "aligned" are light direction and triangle surface normal?
                        float dp = Math.max(0.1f, Util.VectorDotProduct(lightDirection, normal));
                        // Choose Colors as Required
                        triTransformed.col = Util.getColor(dp);

                        // Convert World Space --> View Space
                        Triangle triViewed = new Triangle();
                        triViewed.p[0] = Util.MatrixMultiplyVector(matView, triTransformed.p[0]);
                        triViewed.p[1] = Util.MatrixMultiplyVector(matView, triTransformed.p[1]);
                        triViewed.p[2] = Util.MatrixMultiplyVector(matView, triTransformed.p[2]);
                        triViewed.col = triTransformed.col;
                        triViewed.t = triTransformed.t;

                        // Clip Viewed Triangle against near plane, this could form two additional
                        // triangles
                        returnClip clipResult = Util.TriangleClipAgainstPlane(new Vec3D(0.0f, 0.0f, 0.1f), new Vec3D(0.0f, 0.0f, 1.0f), triViewed);
                        int nClippedTriangles = clipResult.numTris;
                        Triangle[] clipped = clipResult.tris;

                        for (int n = 0; n < nClippedTriangles; n++) {

                            // Project Triangles from 3D --> 2D
                            Triangle triProjected = new Triangle();
                            triProjected.p[0] = Util.MatrixMultiplyVector(matProj, clipped[n].p[0]);
                            triProjected.p[1] = Util.MatrixMultiplyVector(matProj, clipped[n].p[1]);
                            triProjected.p[2] = Util.MatrixMultiplyVector(matProj, clipped[n].p[2]);
                            triProjected.col = clipped[n].col;
                            triProjected.t = clipped[n].t;

                            triProjected.t[0].setU(triProjected.t[0].getU() / triProjected.p[0].getW());
                            triProjected.t[1].setU(triProjected.t[1].getU() / triProjected.p[1].getW());
                            triProjected.t[2].setU(triProjected.t[2].getU() / triProjected.p[2].getW());

                            triProjected.t[0].setV(triProjected.t[0].getV() / triProjected.p[0].getW());
                            triProjected.t[1].setV(triProjected.t[1].getV() / triProjected.p[1].getW());
                            triProjected.t[2].setV(triProjected.t[2].getV() / triProjected.p[2].getW());

                            triProjected.t[0].setW(1.0f / triProjected.p[0].getW());
                            triProjected.t[1].setW(1.0f / triProjected.p[1].getW());
                            triProjected.t[2].setW(1.0f / triProjected.p[2].getW());

                            // Scale into view, we moved the normalising into cartesian space
                            // out of the matrix.vector function from the previous video, so
                            // do this manually
                            triProjected.p[0] = Util.VectorDiv(triProjected.p[0], triProjected.p[0].getW());
                            triProjected.p[1] = Util.VectorDiv(triProjected.p[1], triProjected.p[1].getW());
                            triProjected.p[2] = Util.VectorDiv(triProjected.p[2], triProjected.p[2].getW());

                            // X/Y are inverted so put them back
                            triProjected.p[0].setX(triProjected.p[0].getX() * -1.0f);
                            triProjected.p[1].setX(triProjected.p[1].getX() * -1.0f);
                            triProjected.p[2].setX(triProjected.p[2].getX() * -1.0f);
                            triProjected.p[0].setY(triProjected.p[0].getY() * -1.0f);
                            triProjected.p[1].setY(triProjected.p[1].getY() * -1.0f);
                            triProjected.p[2].setY(triProjected.p[2].getY() * -1.0f);

                            // Offset verts into visible normalised space
                            Vec3D vOffsetView = new Vec3D(1, 1, 0);
                            triProjected.p[0] = Util.VectorAdd(triProjected.p[0], vOffsetView);
                            triProjected.p[1] = Util.VectorAdd(triProjected.p[1], vOffsetView);
                            triProjected.p[2] = Util.VectorAdd(triProjected.p[2], vOffsetView);
                            triProjected.p[0].setX(triProjected.p[0].getX() * 0.5f * (float) canvas.getWidth());
                            triProjected.p[0].setY(triProjected.p[0].getY() * 0.5f * (float) canvas.getHeight());
                            triProjected.p[1].setX(triProjected.p[1].getX() * 0.5f * (float) canvas.getWidth());
                            triProjected.p[1].setY(triProjected.p[1].getY() * 0.5f * (float) canvas.getHeight());
                            triProjected.p[2].setX(triProjected.p[2].getX() * 0.5f * (float) canvas.getWidth());
                            triProjected.p[2].setY(triProjected.p[2].getY() * 0.5f * (float) canvas.getHeight());

                            // Store Triangles for sorting
                            trianglesToRaster.add(triProjected.clone());
                        }
                    }
                }

                Comparator<Triangle> comp = (Triangle t1, Triangle t2) -> {
                    double z1 = (t1.p[0].getZ() + t1.p[1].getZ() + t1.p[2].getZ()) / 3.0;
                    double z2 = (t2.p[0].getZ() + t2.p[1].getZ() + t2.p[2].getZ()) / 3.0;
                    return Double.compare(z1, z2);
                };
                trianglesToRaster.sort(comp.reversed());

                for (Triangle triToRaster: trianglesToRaster) {

                    // Clip triangles against all four screen edges, this could yield
                    // a bunch of triangles
                    Triangle[] clipped;
                    ArrayList<Triangle> listTriangles = new ArrayList<>();

                    // Add initial triangle
                    listTriangles.add(triToRaster);
                    int nNewTriangles = 1;

                    for (int p = 0; p < 4; p++) {

                        int nTrisToAdd;
                        while (nNewTriangles > 0) {

                            // Take triangle from front of queue
                            Triangle test = listTriangles.get(0);
                            listTriangles.remove(0);
                            nNewTriangles--;

                            // Clip it against a plane. We only need to test each
                            // subsequent plane, against subsequent new triangles
                            // as all triangles after a plane clip are guaranteed
                            // to lie on the inside of the plane. I like how this
                            // comment is almost completely and utterly justified
                            returnClip clip = null;

                            switch (p) {
                                case 0: clip = Util.TriangleClipAgainstPlane(new Vec3D(0, 0, 0), new Vec3D(0, 1, 0), test); break;
                                case 1: clip = Util.TriangleClipAgainstPlane(new Vec3D(0, canvas.getHeight() - 1, 0), new Vec3D(0, -1, 0), test); break;
                                case 2: clip = Util.TriangleClipAgainstPlane(new Vec3D(0, 0, 0), new Vec3D(1, 0, 0), test); break;
                                case 3: clip = Util.TriangleClipAgainstPlane(new Vec3D(canvas.getWidth() - 1, 0, 0), new Vec3D(-1, 0, 0), test); break;
                                default: break;
                            }
                            nTrisToAdd = clip.numTris;
                            clipped = clip.tris;

                            // Clipping may yield a variable number of triangles, so
                            // add these new ones to the back of the queue for subsequent
                            // clipping against next planes
                            listTriangles.addAll(Arrays.asList(clipped).subList(0, nTrisToAdd));
                        }
                        nNewTriangles = listTriangles.size();
                    }

                    // Draw the transformed, viewed, clipped, projected, sorted, clipped triangles
                    for (Triangle t : listTriangles) {
                        g2d.setColor(t.col);
                        g2d.fillPolygon(new int[]{(int) t.p[0].getX(), (int) t.p[1].getX(), (int) t.p[2].getX()}, new int[]{(int) t.p[0].getY(), (int) t.p[1].getY(), (int) t.p[2].getY()}, 3);

//                        Util.TexturedTriangle(new ArrayList<>(Arrays.asList((int)t.p[0].getX(), (int)t.p[1].getX(), (int)t.p[2].getX())),
//                                              new ArrayList<>(Arrays.asList((int)t.p[0].getY(), (int)t.p[1].getY(), (int)t.p[2].getY())),
//                                              new ArrayList<>(Arrays.asList(t.t[0].getU(), t.t[1].getU(), t.t[2].getU())),
//                                              new ArrayList<>(Arrays.asList(t.t[0].getV(), t.t[1].getV(), t.t[2].getV())),
//                                              new ArrayList<>(Arrays.asList(t.t[0].getW(), t.t[1].getW(), t.t[2].getW())),
//                                              g2d, sprTex1);

                        g2d.setColor(Color.BLACK);
                        g2d.drawPolygon(new int[]{(int) t.p[0].getX(), (int) t.p[1].getX(), (int) t.p[2].getX()}, new int[]{(int) t.p[0].getY(), (int) t.p[1].getY(), (int) t.p[2].getY()}, 3);
                    }
                }

                graphics = buffer.getDrawGraphics();
                graphics.drawImage(bi, 0, 0, null);
                if (!buffer.contentsLost())
                    buffer.show();

                bi = gc.createCompatibleImage(canvas.getWidth(), canvas.getHeight());
                long end = System.nanoTime();
                elapsedTime = (end - now) / 1000000000.0f;

                if (input.isKey(KeyEvent.VK_ESCAPE))
                    running = false;
                if (input.isKey(KeyEvent.VK_SPACE))
                    vCamera.setY(vCamera.getY() + 8.0f * elapsedTime);
                if (input.isKey(KeyEvent.VK_SHIFT))
                    vCamera.setY(vCamera.getY() - 8.0f * elapsedTime);

                Vec3D vForward = Util.VectorMul(vLookDir, 8.0f * elapsedTime);

                if (input.isKey(KeyEvent.VK_W))
                    vCamera = Util.VectorAdd(vCamera, vForward);
                if (input.isKey(KeyEvent.VK_S))
                    vCamera = Util.VectorSub(vCamera, vForward);
                if (input.isKey(KeyEvent.VK_A))
                    fYaw -= 1.5f * elapsedTime;
                if (input.isKey(KeyEvent.VK_D))
                    fYaw += 1.5f * elapsedTime;

                input.update();

                // fTheta += elapsedTime / 1000000000.0;

                Thread.yield();

            } finally {
                if (graphics != null)
                    graphics.dispose();
                if (g2d != null)
                    g2d.dispose();
            }
        }
        System.exit(0);
    }
}
